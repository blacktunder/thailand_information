import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict:true,
    state:{
        thailandlist:[],
    },
    mutations: {
        addAddress(state, address) {
            state.thailandlist.push(address);
        },
        removeRow(state,i){
            state.thailandlist.splice(i,1);
        },
        addData(state,newdata){
            state.thailandlist.push(newdata);
        },
        updateEdit(state,list){
            state.thailandlist[list.index].province = list.province;
            state.thailandlist[list.index].area = list.area;
            state.thailandlist[list.index].subArea = list.subArea;
            state.thailandlist[list.index].postalCode = list.postalCode;
        },
        sortTable(state,list){
            if(list.reverse && list.sortKey=='Province'){
                function compare(a,b){
                    if(a.province<b.province){return 1;}
                    if(a.province>b.province){return -1;}
                    return 0;
                }
                state.thailandlist.sort(compare);
            }
            if(!list.reverse && list.sortKey=='Province'){
                function compare(a,b){
                    if(a.province>b.province){return 1;}
                    if(a.province<b.province){return -1;}
                    return 0;
                }
                state.thailandlist.sort(compare);
            }
            if(list.reverse && list.sortKey=='Postal code'){
                function compare(a,b){
                    if(a.postalCode<b.postalCode){return 1;}
                    if(a.postalCode>b.postalCode){return -1;}
                    return 0;
                }
                state.thailandlist.sort(compare);
            }
            if(!list.reverse && list.sortKey=='Postal code'){
                function compare(a,b){
                    if(a.postalCode>b.postalCode){return 1;}
                    if(a.postalCode<b.postalCode){return -1;}
                    return 0;
                }
                state.thailandlist.sort(compare);
            }
        }
    }
})