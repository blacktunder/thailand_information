import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store';
import JsonExcel from 'vue-json-excel';

Vue.component('downloadExcel', JsonExcel);
Vue.component('modal', {
  template: '#modal-template'
})

new Vue({
  el: '#app',
  store:store,
  render: h=>h(App)
})

